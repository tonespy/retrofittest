package deliveryscience.retrofittest.Api;


import org.json.JSONObject;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by mac on 6/25/15.
 */
public interface MyApi {

    /*LOGIN*/
    @POST("/login")
    void login(@Query("username") String username, @Query("password") String password, Callback<String> callback);

    /*ORDER*/
    //@Headers("Cache-Control: max-age=640000")
    @GET("/order")
    void getOrder(Callback<String> callback);
}
