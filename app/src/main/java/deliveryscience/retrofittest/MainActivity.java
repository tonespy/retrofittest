package deliveryscience.retrofittest;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;*/

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

import deliveryscience.retrofittest.Api.MyApi;
//import okio.BufferedSink;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;


public class MainActivity extends ActionBarActivity {

    private Button retroTest;
    private TextView respView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retroTest = (Button) findViewById(R.id.retroTest);
        respView = (TextView) findViewById(R.id.respView);

        retroTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    /*String test = run("http://posdemoapi.imspro.co/login");
                    Log.d("TEST", "");
                    respView.setText(test);*/
                    //testRetrof("admin@admin.com", "password");
                    testOrder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void testRetrof(String username, String password) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://posdemoapi.imspro.co")
                .setConverter(new StringConverter())
                .build();

        MyApi myTask = restAdapter.create(MyApi.class);
        myTask.login(username, password, new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                Log.d("Responses", s);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    public void testOrder() {
        Log.d("Responses", "Here");
        final String token = "Bearer" + "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmRlcnMiOltdLCJ1c2VybmFtZSI6ImFkbWluQGFkbWluLmNvbSIsImlzRGVsZXRlZCI6ZmFsc2UsImNyZWF0ZWRBdCI6IjIwMTUtMDYtMjVUMDc6MjU6MzUuNDc0WiIsInVwZGF0ZWRBdCI6IjIwMTUtMDYtMjVUMDc6MjU6MzUuNDc0WiIsImlkIjoiNTU4YmFjZWY1Y2FlNjFhMDVhNjc3M2IzIiwiaWF0IjoxNDM1MjM3OTM1LCJleHAiOjE0MzUyNDUxMzV9.pfrq9rFtsq9xQ9kBQcnVpaN3mvdIC1TRbZ0Thdq9xos";

        RequestInterceptor interceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization",token);
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://posdemoapi.imspro.co")
                .setRequestInterceptor(interceptor)
                .setConverter(new StringConverter())
                .build();

        MyApi myTask = restAdapter.create(MyApi.class);
        myTask.getOrder(new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                Log.d("Responses", s);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    static class StringConverter implements Converter {

        @Override
        public Object fromBody(TypedInput typedInput, Type type) throws ConversionException {
            String text = null;
            try {
                text = fromStream(typedInput.in());
            } catch (IOException ignored) {/*NOP*/ }

            return text;
        }

        @Override
        public TypedOutput toBody(Object o) {
            return null;
        }

        public static String fromStream(InputStream in) throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }
            return out.toString();
        }
    }

    /*public String run(String url) throws Exception {
        final String[] resp = {""};
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormEncodingBuilder()
                .add("username", "admin@admin.com")
                .add("password", "password").build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        //Response response = client.newCall(request).execute();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e("Failed to execute", request.toString() + " " + e.toString());
                resp[0] = request.toString();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code " + response);
                }
                resp[0] = response.body().string();
                Log.d("Responses:", resp[0]);
            }
        });
        Log.d("Responses:", resp[0]);
        return resp[0];
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
